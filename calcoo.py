#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys


class Calculadora():

    def __init__(self, operando1, operacion, operando2):

        self.operando1 = operando1
        self.operando2 = operando2
        self.operacion = operacion

    def suma(self):
        return float(self.operando1) + float(self.operando2)

    def resta(self):
        return float(self.operando1) - float(self.operando2)

    def operar(self):

        if self.operacion == 'suma':
            return self.suma()

        elif self.operacion == 'resta':
            return self.resta()
        else:
            print("No es valida esa operacion")

if __name__ == "__main__":
    try:
        operando1 = float(sys.argv[1])
        operacion = sys.argv[2]
        operando2 = float(sys.argv[3])

    except ValueError:
        sys.exit("Error: El parametro no es numerico")

    objeto = Calculadora(operando1, operacion, operando2)
    print(objeto.operar())
