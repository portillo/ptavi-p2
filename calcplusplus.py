#!/usr/bin/python3
# -*- coding: utf-8 -*-

#   operacion, operando1, operando2, operando3,..., operandoN

import sys
import calcoohija
import csv

if __name__ == "__main__":

    if len(sys.argv) != 2:
        sys.exit("usage: calcplusplus.py fichero")

    with open("fichero.csv") as lineas:

        fichero = csv.reader(lineas)

        for operador in fichero:
            operar = operador[0]
            resultado = operador[1]

            for numero in operador[2:]:
                objeto = calcoohija.CalculadoraHija(resultado, operar, numero)
                resultado = objeto.operar()
            print(resultado)
