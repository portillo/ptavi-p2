#!/usr/bin/python3
# -*- coding: utf-8 -*-

#   operacion, operando1, operando2, operando3,..., operandoN

import sys
import calcoohija

if __name__ == "__main__":

    if len(sys.argv) != 2:
        sys.exit("usage: calcplus.py fichero.csv")
    fichero = open(sys.argv[1])
    contenido = fichero.readlines()

    for operador in contenido:
        interior = operador.split(",")
        operar = interior[0]    # posicion 0, variable fija

        resultado = interior[1]

        for numero in interior[2:]:
            objeto = calcoohija.CalculadoraHija(resultado, operar, numero)
            resultado = objeto.operar()
        print(resultado)

    fichero.close()
